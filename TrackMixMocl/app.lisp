#-mocl (declaim (declaration call-in))
(rt:enable-objc-reader)

(defvar *volume* 0.0)

(declaim (call-in set-volume))
(defun set-volume (vol)
  (setf *volume* vol))

(declaim (call-in get-volume :result float))
(defun get-volume ()
  *volume*)

(declaim (call-in update-volume))
(defun update-volume (self vol)
  (setf *volume* vol)
  @(self textField :setFloatValue *volume*))
