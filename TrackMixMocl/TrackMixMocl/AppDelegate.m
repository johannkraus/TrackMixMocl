//
//  AppDelegate.m
//  TrackMixMocl
//
//  Created by Johann Kraus on 29/07/14.
//  Copyright (c) 2014 Johann Kraus. All rights reserved.
//

#import "AppDelegate.h"

// the c way
// import track class header
// #import "Track.h"

// the lisp way
// import mocl header
#import "mocl.h"

@implementation AppDelegate

@synthesize textField;
@synthesize slider;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    cl_init();
    
    // the c way
    // create instance of track object and call updateUI to sync view with model
    //    Track *aTrack = [[Track alloc] init];
    //    [self setTrack:aTrack];
    //    [self updateUserInterface];

    // the lisp way
    // initialize volume and call updateUI to sync view with model
    set_volume(5.0);
    [self updateUserInterface];
}

- (IBAction)mute:(id)sender {
    // log code
    NSLog(@"received a mute: message");

    // the c way
    // set track's value to 0 and call updateUI to sync view with model
    // [self.track setVolume:0.0];
    // [self updateUserInterface];

    // the lisp way
    // set volume to 0 and call updateUI to sync view with model
    set_volume(0.0);
    [self updateUserInterface];

    // or do both in lisp, i.e. set view without the appdelegate conroller
    // update_volume(self, 0.0);
}

- (IBAction)takeFloatValueForVolumeFrom:(id)sender {
    // log code
    NSString *senderName = nil;
    if (sender == self.textField) {
        senderName = @"textField";
    }
    else {
        senderName = @"slider";
    }
    NSLog(@"%@ sent takeFloatValueForVolumeFrom: with value %1.2f",
          senderName, [sender floatValue]);
    
    // the c way
    // get value from sender, set track's value and finally call updateUI to sync view with model
    // float newValue = [sender floatValue];
    // [self.track setVolume:newValue];
    // [self updateUserInterface];

    // the lisp way
    // get value from sender, set volume and finally call updateUI to sync view with model
    float newValue = [sender floatValue];
    set_volume((double)newValue); // why is this typecast needed?
    [self updateUserInterface];

    // or do both in lisp, i.e. set view without the appdelegate conroller
    // float newValue = [sender floatValue];
    // update_volume(self, (double)newValue); // why is this typecast needed?
}

- (void)updateUserInterface {
    // the c way
    // get value from model and set values in UI
    // float volume = [self.track volume];
    // [self.textField setFloatValue:volume];
    // [self.slider setFloatValue:volume];

    // the lisp way
    // get volume and set values in UI
    float volume = get_volume();
    [self.textField setFloatValue:volume];
    [self.slider setFloatValue:volume];
}
@end
