//
//  Track.h
//  TrackMixMocl
//
//  Created by Johann Kraus on 29/07/14.
//  Copyright (c) 2014 Johann Kraus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Track : NSObject

@property (assign) float volume;

@end
