//
//  AppDelegate.h
//  TrackMixMocl
//
//  Created by Johann Kraus on 29/07/14.
//  Copyright (c) 2014 Johann Kraus. All rights reserved.
//

#import <Cocoa/Cocoa.h>

// forward declaration of track class
@class Track;

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

// outlets
@property (weak) IBOutlet NSTextField *textField;
@property (weak) IBOutlet NSSlider *slider;

// actions
- (IBAction)mute:(id)sender;
- (IBAction)takeFloatValueForVolumeFrom:(id)sender;

// function to update UI
- (void)updateUserInterface;

// add declaration of track property
@property (strong) Track *track;

@end
