//
//  main.m
//  TrackMixMocl
//
//  Created by Johann Kraus on 29/07/14.
//  Copyright (c) 2014 Johann Kraus. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
